<?php
/**
 * Plugin Name:       Micro Plugin
 * Description:       Un plugin qui affiche les posts d'autres blogs.
 * Version:           1.0
 * Requires at least: 5.6.2
 * Requires PHP:      7.2
 * Author:            Mario
 * Text Domain:       micro-plugin
 */

// --------------------- Config --------------------- //

include_once 'Parsedown.php';

function micro_create_menu_section() {
    add_menu_page(
        'Micro Plugin',                             // Titre
        'Micro Plugin',                             // Nom dans le menu
        'manage_options',                           // Droits nécessaires pour voir le plugin
        'micro-plugin/micro-plugin.php',            // Slug
        'micro_afficher_plugin',                    // Fonction appelée pour afficher le contenu
        '',                                         // Chemin vers l'icone
        86                                          // Position dans le menu
    );
}

add_action( 'admin_menu', 'micro_create_menu_section' );

function micro_settings_init() {
    register_setting('micro_options', 'blog_name');
    register_setting('micro_options', 'blog_url');
}

add_action('admin_init', 'micro_settings_init');


function micro_afficher_plugin() {
    ?>
    <h1>Micro Plugin</h1>
    <form action="options.php" method="post">
        <?php settings_fields('micro_options'); ?>
        <label for="blog_name">Nom du blog</label>
        <input type="text" name="blog_name" id="blog_name" value="<?php echo get_option('blog_name'); ?>">
        <label for="blog_url">URL du blog</label>
        <input type="text" name="blog_url" id="blog_name" value="<?php echo get_option('blog_url'); ?>">
        <?php submit_button(); ?>
    </form>
    <?php
}

// --------------------- Plugin --------------------- //

function micro_plugin_get_lists() {
    $list = array (get_option('blog_url'));
    return $list;
}

function micro_plugin_get_posts() {
    if (is_page('Blogs')) {
        $Parsedown = new Parsedown();
        $list_of_lists = micro_plugin_get_lists();
        foreach ($list_of_lists as $link) {
    
            $json_list = file_get_contents($link);
            $obj_list = json_decode($json_list);
    
            foreach ($obj_list->post as $post_link) {
    
                $json_post = file_get_contents($post_link);
                $post = json_decode($json_post);
                ?>
    
                <div class="micro_plugin_post">
                    <p class="micro_plugin_contenu"><?php echo $Parsedown->text($post->contenu); ?></p>
                    <p class="micro_plugin_date"><?php echo $post->date; ?></p>
                </div>
    
                <?php
            }
        }
    }
}

add_action('the_content', 'micro_plugin_get_posts');

function micro_plugin_css() {
	?>

	<style type='text/css'>
	.micro_plugin_post {
		background-color: slategrey;
        padding: 40px;
	}
    .micro_plugin_date {
        background-color: grey;
        text-align: center;
        margin-top: 40px;
    }
	</style>

	<?php
}

add_action( 'wp_head', 'micro_plugin_css' );
